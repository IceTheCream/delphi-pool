unit reference;

interface

uses
  SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, StdCtrls, Buttons, ExtCtrls, DB, sqldb,
  refedit,
  tlib, tlzdb, twin, tcfg;

type

  { TfmReference }

  TfmReference = class(TForm)
    ButtonPanel: TPanel;
    bbtAdd: TBitBtn;
    bbtEdit: TBitBtn;
    bbtDel: TBitBtn;
    bbtExit: TBitBtn;
    dsqrReference: TDatasource;
    dbgReference: TDBGrid;
    dsaqrReference: TDataSource;
    edSearch: TEdit;
    qrReference: TSQLQuery;
    qrReferenceExt: TSQLQuery;
    TransRefExt: TSQLTransaction;
    TransRef: TSQLTransaction;
    procedure bbtExitClick(Sender: TObject);
    procedure bbtAddClick(Sender: TObject);
    procedure bbtEditClick(Sender: TObject);
    procedure bbtDelClick(Sender: TObject);
    procedure FormClose(Sender: TObject; {%H-}var CloseAction: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure edSearchChange(Sender: TObject);
    procedure dbgReferenceKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
    masSQLArray    : TSQLArray;
    mblChanged     : Boolean;
    msHeader       : String;
    miMaxLength    : Integer;
  public
    { Public declarations }
    function viewReference() : Boolean;
    procedure setSQLArray(pasSQLArray : TSQLArray);
    function isChanged() : Boolean;
    procedure setHeader(psHeader : String);
    procedure loadConfig;
    procedure saveConfig;
    function  getMaxLength() : Integer;
    procedure State();

  end;

{I '.ppconst.inc'}

var
  fmReference: TfmReference;

implementation

{$R *.lfm}

uses main,refselect;

const csErrOpenTableExceptionCatched        = ' 1001';
      csErrOpenTableExceptionCatchedMsg     =
        'При попытке открытия БД возникла исключительная ситуация.'+
        csErrorDescriptionMsg;

{%H-} csErrInsertingTableExceptionCatched    = ' 1002';
{%H-} csErrInsertingTableExceptionCatchedMsg =
        'В процессе сохранения данных в БД возникла исключительная ситуация.'+
          csErrorDescriptionMsg;

      csErrDeletingTableExceptionCatched     = ' 1003';
      csErrDeletingTableExceptionCatchedMsg  =
        'В процессе удаления данных в БД возникла исключительная ситуация.'+
          csErrorDescriptionMsg;

      csErrUpdatingTableExceptionCatched     = ' 1004';
      csErrUpdatingTableExceptionCatchedMsg  =
        'В процессе изменения данных в БД возникла исключительная ситуация.'+
          csErrorDescriptionMsg;


{R *.dfm}

procedure TfmReference.bbtAddClick(Sender: TObject);
begin

  fmRefEdit.setSQLArray(masSQLArray);
  fmRefEdit.DoAppend();
  mblChanged:=fmRefEdit.ModalResult=mrOk;
  qrReference.Locate('fname',fmRefEdit.edRefItem.Text,[loCaseInsensitive]);
  State;
end;


procedure TfmReference.bbtDelClick(Sender: TObject);
//var liCount : Integer;
begin

  try

    (* В связи с введением fstatus этот кусок не нужен
    initializeQuery(qrReferenceExt,masSQLArray[ciCheckSQLIdx]);
    qrReferenceExt.Params.ParamByName('pid').Value:=
      qrReference.FieldByName('id').AsInteger;
    qrReferenceExt.Open;
    liCount:=qrReferenceExt.RecordCount;
    qrReferenceExt.Close;
    TransRefExt.Commit;

    if liCount>0 then
    begin

       Notice('Внимание!',
         'Невозможно удалить этот элемент справочника,'+LF+
         ' так как он уже используется!');

    end
    else
    begin
    *)
    if AnsiUpperCase(qrReference.FieldByName('fname').AsString)='<ПУСТО>' then
    begin

      Notice('Внимание!','Этот элемент справочника не удаляется.');
    end
    else
    begin

      if WarnYesNo('Внимание!','Вы действительно хотите удалить этот элемент справочника?') then
      begin

        //restartTransaction(TransRefExt);
        initializeQuery(qrReferenceExt,masSQLArray[ciDeleteSQlIdx]);
        qrReferenceExt.Params.ParamByName('pid').Value:=
         qrReference.FieldByName('id').AsInteger;
        qrReferenceExt.ExecSQL;
        TransRefExt.Commit;
        refreshQuery(qrReference);
      end;
    end;
    //end;
  except

    on E : Exception do
    begin

      MainForm.processException(csErrorMsg+csErrDeletingTableExceptionCatched,
                 csErrDeletingTableExceptionCatchedMsg,E.Message);
      TransRefExt.Rollback;
    end;
  end;
  State();
end;


procedure TfmReference.bbtEditClick(Sender: TObject);
var liCount : Integer;
begin


  initializeQuery(qrReferenceExt,masSQLArray[ciCheckSQLIdx]);
  qrReferenceExt.Params.ParamByName('pid').Value:=
     qrReference.FieldByName('id').AsInteger;
  try

    qrReferenceExt.Open;
    liCount:=qrReferenceExt.RecordCount;
    qrReferenceExt.Close;
    TransRefExt.Commit;
    if liCount>0 then
    begin

      Notice('Внимание!',
         'Невозможно изменить этот элемент справочника,'+LF+
         ' так как он уже используется!');
    end
    else
    begin

      if AnsiUpperCase(qrReference.FieldByName('fname').AsString)='<ПУСТО>' then
      begin

        Notice('Внимание!','Этот элемент справочника не редактируется.');
      end
      else
      begin


        fmRefEdit.setSQLArray(masSQLArray);
        fmRefEdit.DoEdit();
        mblChanged:=fmRefEdit.ModalResult=mrOk;
      end;
    end;
  except

    on E : Exception do
    begin

       MainForm.processException(csErrorMsg+csErrUpdatingTableExceptionCatched,
                  csErrUpdatingTableExceptionCatchedMsg,E.Message);
    end;
  end;
end;


procedure TfmReference.bbtExitClick(Sender: TObject);
begin

  Close();
end;


procedure TfmReference.dbgReferenceKeyPress(Sender: TObject; var Key: Char);
begin

  fmReference.FocusControl(edSearch);
  edSearch.Text:=Key;
  edSearch.SelStart:=1;
  edSearch.SelLength:=0;
end;


procedure TfmReference.edSearchChange(Sender: TObject);
begin
  
  qrReference.Locate('fname',edSearch.Text,[loCaseInsensitive,loPartialKey]);
end;


procedure TfmReference.FormActivate(Sender: TObject);
begin

  mblChanged:=False;
  miMaxLength:=fmRefSelect.getMaxLength();
  State();
end;


procedure TfmReference.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin

  saveConfig();
  inherited;
end;


procedure TfmReference.FormCreate(Sender: TObject);
begin

  inherited;
  loadConfig();
end;


function TfmReference.isChanged: Boolean;
begin

  Result:=mblChanged;
end;


procedure TfmReference.loadConfig;
begin

  IniOpen(g_sProgrammFolder+csEtcPath+Self.Name+'.ini');
  IniReadFormEx(Self);
  IniReadColumns(dbgReference);
  IniClose();
end;


function TfmReference.viewReference : Boolean;
begin

  Result:=False;
  Self.Caption:='Справочник '+msHeader;

  try

    initializeQuery(qrReference,masSQLArray[ciSelectSQLIdx]);
    qrReference.Open;
    Result:=ShowModal=mrOk;
  except

    on E:Exception do
    begin

      MainForm.processException(csErrorMsg+csErrOpenTableExceptionCatched,
        csErrOpenTableExceptionCatchedMsg,E.Message);
    end;
  end;
end;


procedure TfmReference.setHeader(psHeader: String);
begin

  msHeader:=psHeader;
end;


procedure TfmReference.setSQLArray(pasSQLArray: TSQLArray);
begin

  masSQLArray:=pasSQLArray;
end;


procedure TfmReference.saveConfig;
begin

  if IsFolderExists(g_sProgrammFolder+csEtcPath) then
  begin
  
    IniOpen(g_sProgrammFolder+csEtcPath+Self.Name+'.ini');
    iniSaveFormEx(Self);
    IniSaveColumns(dbgReference);
    IniClose();
  end;
end;


function TfmReference.getMaxLength: Integer;
begin

  Result:=miMaxLength;
end;


procedure TfmReference.State;
begin

  bbtDel.Enabled:=qrReference.RecordCount>0;
  bbtEdit.Enabled:=qrReference.RecordCount>0;
end;


end.
