unit login;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  Buttons, StdCtrls, tlib, tcfg, tlcl;

type

  { TfmLogin }

  TfmLogin = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    edPassword: TEdit;
    edLogin: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Panel1: TPanel;
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var {%H-}CloseAction: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormHide(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { private declarations }
    procedure saveConfig;
    procedure loadConfig;
  public
    { public declarations }
  end;

var
  fmLogin: TfmLogin;

implementation

{$R *.lfm}

uses main;

{$i const.inc}

{ TfmLogin }

procedure TfmLogin.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin

  saveConfig();
  inherited;
end;


procedure TfmLogin.FormActivate(Sender: TObject);
begin

  FocusControl(edPassword);
end;


procedure TfmLogin.FormCreate(Sender: TObject);
begin

  loadConfig();
  inherited;
end;


procedure TfmLogin.FormHide(Sender: TObject);
begin

  MainForm.setState(psIdle);
end;


procedure TfmLogin.FormShow(Sender: TObject);
begin

  MainForm.setState(psBusy);
end;


procedure TfmLogin.loadConfig;
begin

  IniOpen(g_sProgrammFolder + csEtcPath + Self.Name + '.ini');
  IniReadFormEx(Self);
  IniClose();
end;


procedure TfmLogin.saveConfig;
begin

  if IsFolderExists(g_sProgrammFolder + csEtcPath) then
  begin

    IniOpen(g_sProgrammFolder + csEtcPath + Self.Name + '.ini');
    iniSaveFormEx(Self);
    IniClose();
  end;
end;


end.

