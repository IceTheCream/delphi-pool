unit protect;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  ExtCtrls, Buttons,
  twin, tcfg, tlib;

type

  { TfmProtect }

  TfmProtect = class(TForm)
    bbtCancel: TBitBtn;
    bbtOk: TBitBtn;
    edPassword: TEdit;
    Label1: TLabel;
    Panel1: TPanel;
    procedure bbtOkClick(Sender: TObject);
    procedure FormClose(Sender: TObject; {%H-} var CloseAction: TCloseAction);
    procedure FormCreate(Sender: TObject);
  private
    { private declarations }
    msProgramFilesFolder : String;

    procedure loadConfig;
    procedure saveConfig;
  public
    { public declarations }
    procedure CheckInstallation;
    function touch(psFileName : String) : Boolean;
  end;

{$i const.inc}

var
  fmProtect: TfmProtect;

implementation

{$R *.lfm}

uses main;

{ TfmProtect }

procedure TfmProtect.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin

  saveConfig();
  inherited;
end;


procedure TfmProtect.bbtOkClick(Sender: TObject);
begin

  //***** Пароль набрали правильно?
  if UpperCase(edPassword.Text) = UpperCase(csSecretPhrase) then
  begin

    if not touch(msProgramFilesFolder+csSecretFile1) then
      if not touch(msProgramFilesFolder+csSecretFile2) then
        FatalError(UTF8ToSys('Ошибка!'),UTF8ToSys('Невозможно привязать программу!'));
  end
  else
  begin

    FatalError(UTF8ToSys('Ошибка!'),UTF8ToSys('Пароль неверный!'));
    Application.Terminate;
  end;
end;


procedure TfmProtect.FormCreate(Sender: TObject);
begin

  inherited;
  loadConfig();
  msProgramFilesFolder:=GetProgramFilesFolder();
end;


procedure TfmProtect.loadConfig;
begin

  IniOpen(g_sProgrammFolder+csEtcPath+Self.Name+'.ini');
  IniReadFormEx(Self);
  IniClose();
end;


procedure TfmProtect.saveConfig;
begin

  if IsFolderExists(g_sProgrammFolder+csEtcPath) then
  begin

    IniOpen(g_sProgrammFolder+csEtcPath+Self.Name+'.ini');
    iniSaveFormEx(Self);
    IniClose();
  end;
end;


procedure TfmProtect.CheckInstallation;
begin

  //***** Проверим первый файл
  if FileExists(msProgramFilesFolder+csSecretFile1) then
    Exit;

  //***** Проверим второй файл
  if FileExists(msProgramFilesFolder+csSecretFile2) then
    Exit;

  ShowModal();
end;


function TfmProtect.touch(psFileName: String): Boolean;
begin

  Result:=False;
  if not FileExists(psFileName) then
  begin

    try

      FileCreateUTF8(psFileName);
      Result:=FileExists(psFileName);
    except

      Result:=False;
    end;
  end;
end;


end.

