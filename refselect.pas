unit refselect;

interface

uses
  SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, StdCtrls, Buttons, ExtCtrls,
  tlzdb, tcfg, tlib, sqldb, db;

type

  { TfmRefSelect }

  TfmRefSelect = class(TForm)
    dsqrReference: TDatasource;
    Panel1: TPanel;
    bbtOk: TBitBtn;
    bbtCancel: TBitBtn;
    dbgRefSelect: TDBGrid;
    bbtReference: TBitBtn;
    Panel2: TPanel;
    edSearch: TEdit;
    qrReference: TSQLQuery;
    TransRef: TSQLTransaction;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; {%H-}var CloseAction: TCloseAction);
    procedure bbtReferenceClick(Sender: TObject);
    procedure edSearchChange(Sender: TObject);
  private
    { Private declarations }
    masSQLRefArray  : TSQLArray;
    msHeader : String;
    miLastSelectedID : Integer;
    miMaxLength : Integer;

  public
    { Public declarations }
    function  getLastSelectedID : Integer;
    procedure setSQLArray(pasSQLRefArray : TSQLArray);
    procedure setHeader(psHeader : String);
    function  viewRefSelect(piID : Integer) : Boolean;
    procedure loadConfig();
    procedure saveConfig();
    procedure setMaxLength(piMaxLength : Integer);
    function  getMaxLength() : Integer;
  end;

{$I const.inc}
const csErrOpenTableExceptionCatched       = ' 201';
      csErrOpenTableExceptionCatchedMsg    =
        'При попытке открытия БД возникла исключительная ситуация.'+
        csErrorDescriptionMsg;


var
  fmRefSelect: TfmRefSelect;

implementation

uses main, reference;

{$R *.lfm}


procedure TfmRefSelect.bbtReferenceClick(Sender: TObject);
begin

  fmReference.setHeader(msHeader);
  fmReference.setSQLArray(masSQLRefArray);
  fmReference.viewReference;
  refreshQuery(fmRefSelect.qrReference,False);
  qrReference.Locate('id',fmReference.qrReference.FieldByName('id').AsInteger,[]);
end;


procedure TfmRefSelect.edSearchChange(Sender: TObject);
begin

  qrReference.Locate('fname',edSearch.Text,[loCaseInsensitive,loPartialKey]);
end;


procedure TfmRefSelect.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin

  saveConfig();
  inherited;
end;


procedure TfmRefSelect.FormCreate(Sender: TObject);
begin

  inherited;
  loadConfig();
end;


function TfmRefSelect.getLastSelectedID: Integer;
begin

  Result:=miLastSelectedID;
end;


procedure TfmRefSelect.loadConfig;
begin

  IniOpen(g_sProgrammFolder+csEtcPath+Self.Name+'.ini');
  IniReadFormEx(Self);
  IniReadColumns(dbgRefSelect);
  IniClose;
end;


procedure TfmRefSelect.saveConfig;
begin

  if IsFolderExists(g_sProgrammFolder+csEtcPath) then
  begin
    IniOpen(g_sProgrammFolder+csEtcPath+Self.Name+'.ini');
    iniSaveFormEx(Self);
    IniSaveColumns(dbgRefSelect);
    IniClose;
  end;
end;


procedure TfmRefSelect.setMaxLength(piMaxLength: Integer);
begin

  miMaxLength:=piMaxLength;
end;


function TfmRefSelect.getMaxLength: Integer;
begin

  Result:=miMaxLength;
end;


procedure TfmRefSelect.setHeader(psHeader: String);
begin

  msHeader:=psHeader;
end;


procedure TfmRefSelect.setSQLArray(pasSQLRefArray: TSQLArray);
begin

  masSQLRefArray:=pasSQLRefArray;
end;


function TfmRefSelect.viewRefSelect(piID : Integer) : Boolean;
begin

  Result:=False;
  Self.Caption:='Селектор '+msHeader;

  initializeQuery(qrReference,masSQLRefArray[ciSelectSQLIdx]);
  try

    fmRefSelect.qrReference.Open;
    fmRefSelect.qrReference.Locate('id',piID,[]);
    Result:=fmRefSelect.ShowModal=mrOk;
    miLastSelectedID:=fmRefSelect.qrReference.FieldByName('id').AsInteger;
    fmRefSelect.qrReference.Close;

  except

    on E:Exception do
    begin

      MainForm.processException(csErrorMsg+csErrOpenTableExceptionCatched,
        csErrOpenTableExceptionCatchedMsg,E.Message);
    end;
  end;
end;


end.
