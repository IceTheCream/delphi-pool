unit refedit;

interface

uses
  SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, sqldb, db,
  tlzdb, tstr, tcfg, tlib, twin;

type

  { TfmRefEdit }

  TfmRefEdit = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    edRefItem: TEdit;
    bbtOk: TBitBtn;
    bbtCancel: TBitBtn;
    qrReferenceExt: TSQLQuery;
    TransRef: TSQLTransaction;
    procedure bbtOkClick(Sender: TObject);
    procedure FormClose(Sender: TObject; {%H-} var CloseAction: TCloseAction);
    procedure FormCreate(Sender: TObject);
    //procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure bbtCancelClick(Sender: TObject);
  private
    { Private declarations }
    moMode      : TDBMode;
    masSQLArray : TSQLArray;
    miMaxLength : Integer;

  public
    { Public declarations }
    procedure setSQLArray(pasSQLArray : TSQLArray);
    procedure DoAppend;
    procedure DoEdit;
    procedure InitData;
    procedure LoadData;
    procedure StoreData;
    function ValidateData : Boolean;
    procedure loadConfig;
    procedure saveConfig;
  end;

const
  {i errors.inc}

  csErrEmptyLine                          = ' R01';
  csErrEmptyLineMsg                       =
    'Некорректные данные - пустая строка';

  csErrTooLongLine                          = ' R02';
  csErrTooLongLineMsg                       =
    'Некорректные данные - слишком длинная строка. Допустимо не более %d';


  csErrDoubleFound                        = ' R03';
  csErrDoubleFoundMsg                     =
    'В справочнике уже имеется такая запись.';


var
  fmRefEdit: TfmRefEdit;



implementation

uses reference, main;


{$R *.lfm}

{ TfmRefEdit }

procedure TfmRefEdit.bbtCancelClick(Sender: TObject);
begin

  ModalResult:=mrCancel;
end;


procedure TfmRefEdit.bbtOkClick(Sender: TObject);
begin

  if ValidateData() then
  begin

    try

      restartTransaction(TransRef);
      StoreData();
      qrReferenceExt.ExecSQL;
      TransRef.Commit;
      restartTransaction(fmReference.TransRef);
      refreshQuery(fmReference.qrReference);

    except

      on E: Exception do
      begin

        MainForm.processException(csErrorMsg+csErrUpdatingTableExceptionOccured,
          csErrUpdatingTableExceptionOccuredMsg,E.Message);
        TransRef.Rollback;
      end;
    end;
    ModalResult:=mrOk
  end
end;


procedure TfmRefEdit.DoAppend;
begin

  miMaxLength:=fmReference.getMaxLength();
  initializeQuery(qrReferenceExt,masSQLArray[ciInsertSQLIdx]);
  moMode:=dmInsert;
  InitData();
  ShowModal();
end;


procedure TfmRefEdit.DoEdit;
begin

  miMaxLength:=fmReference.getMaxLength();
  initializeQuery(qrReferenceExt,masSQLArray[ciUpdateSQLIdx]);
  LoadData();
  moMode:=dmUpdate;
  ShowModal();
end;


procedure TfmRefEdit.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin

  saveConfig();
  inherited;
end;


procedure TfmRefEdit.FormCreate(Sender: TObject);
begin

  inherited;
  loadConfig();
end;


procedure TfmRefEdit.InitData;
begin

  edRefItem.Text:='';
end;


procedure TfmRefEdit.loadConfig;
begin

  IniOpen(g_sProgrammFolder+csEtcPath+Self.Name+'.ini');
  IniReadFormEx(Self);
  IniClose();
end;


procedure TfmRefEdit.LoadData;
begin

  edRefItem.Text:=fmReference.qrReference.FieldByName('fname').AsString;
end;


procedure TfmRefEdit.saveConfig;
begin

  if IsFolderExists(g_sProgrammFolder+csEtcPath) then
  begin
    IniOpen(g_sProgrammFolder+csEtcPath+Self.Name+'.ini');
    iniSaveFormEx(Self);
    IniClose();
  end;
end;


procedure TfmRefEdit.setSQLArray(pasSQLArray: TSQLArray);
begin

  masSQLArray:=pasSQLArray;
end;


procedure TfmRefEdit.StoreData;
begin

  qrReferenceExt.Params.ParamByName('pname').Value:=edRefItem.Text;
  if moMode=dmUpdate then
  begin
  
    qrReferenceExt.Params.ParamByName('pid').Value:=
      fmReference.qrReference.FieldByName('id').AsInteger;
  end;
end;


function TfmRefEdit.ValidateData: Boolean;
begin

  Result:=False;
  if IsEmpty(edRefItem.Text) then
  begin

    MainForm.processFatalError(csErrorMsg+csErrEmptyLine,csErrEmptyLineMsg);
  end
  else
  begin

    if Length(edRefItem.Text)>miMaxLength then
    begin

      MainForm.processFatalError(csErrorMsg+csErrTooLongLine,Format(csErrTooLongLineMsg,[miMaxLength]));
    end
    else
    begin

      //***** Проверим на дупли
      if fmReference.qrReference.Locate('fname',edRefItem.Text,[loCaseInsensitive]) then
      begin

        MainForm.processFatalError(csErrorMsg+csErrDoubleFound,csErrDoubleFoundMsg);
      end
      else
      begin

        Result:=True;
      end;
    end;
  end;
end;


end.
